FROM node:10.15-alpine

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install --production --frozen-lockfile
COPY . ./

CMD ["yarn", "start"]
